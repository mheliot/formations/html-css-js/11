Vue.createApp({

    data() {
        return {
            title: 'Gestion des articles',
            articles: []
        }
    },

    methods: {        

        addArticle(article) {
            this.articles.push(article)
        },

        closeModale() {
            this.$refs.addModale.close()
        },

        openModale() {
            this.$refs.addModale.showModal()
        },

        formatDate(dateISO) {
            return new Date(dateISO).toLocaleDateString("fr-FR")
        },

        formatPrice(price) {
            return Number(price).toFixed(2) + ' €'
        },

        handleAddForm() {
            var data = new FormData(this.$refs.addForm)
            var article = {};
            data.forEach(function(value, key) {
                article[key] = value;
            });
            article.createdAt = new Date()
            this.addArticle(article)
            this.closeModale()
            this.$refs.addForm.reset()
        }
    },

    created() {
        fetch('http://my-json-server.typicode.com/mathieuheliot/courses-api/books')
            .then( response => response.json() )
            .then( data => this.articles = data )
            .catch( error => console.error(error) )
    }

}).mount('#app')